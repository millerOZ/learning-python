#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import socket
from socket import *

def main():
    
    equipo = raw_input("Ingrese el dominio o ip que desea escanear: ")
    ipequipo = gethostbyname(equipo)
    print 'Comenzado el escaneo en la ip', ipequipo;
    
    for puertos in range(19,1000):
        cliente = socket(AF_INET, SOCK_STREAM)
        resultado = cliente.connect_ex((ipequipo,puertos))
        if( resultado == 0):
            print 'Puerto %d: abierto' %(puertos);
            cliente.close()
    
if __name__ == "__main__":
	main()
    
